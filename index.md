# Workshop on heterogeneous data and Federated Learning

Description of the workshop goes here

## Material usage

Herein you will find some material that will be developed during the workshop. Some of the material corresponds to text and images that you can download in the upper right corner <i class="fas fa-download"></i> 



## Launch my notebooks

You can have an environment for yourself by clicking here: [![Binder](https://mybinder.org/badge_logo.svg)](http://bit.ly/3iahdfl)